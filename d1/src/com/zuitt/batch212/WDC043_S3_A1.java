package com.zuitt.batch212;

import java.util.Scanner;

public class WDC043_S3_A1 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // using while loop
        System.out.println("Using while loop:");
        int num = 0;
        int answer = 1;
        int counter = 1;

        try {
            System.out.println("Input an integer whose factorial will be computed");
            num = input.nextInt();
        }
        catch (Exception e) {
            System.out.println("Invalid input. Input should be an integer");
        }

        if (num > 0) {
            while (counter <= num) {
                answer *= counter;
                counter++;
            }
            System.out.println("The factorial of " + num + " is " + answer);
        } else if (num == 0) {
            answer = 1;
            System.out.println("The factorial of " + num + " is " + answer);
        } else if (num < 0) {
            System.out.println("The factorial of negative integer cannot be computed.");
        }


        // using for loop
        System.out.println("\nUsing for loop:");
        int num2 = 0;
        int answer2 = 1;

        try {
            System.out.println("Input an integer whose factorial will be computed");
            num2 = input.nextInt();
        }
        catch (Exception e) {
            System.out.println("Invalid input. Input should be an integer");
        }

        if (num2 > 0) {
            for (int counter2 = 1; counter2 <= num2; counter2++) {
                answer2 *= counter2;
            }
            System.out.println("The factorial of " + num2 + " is " + answer2);
        } else if (num2 == 0) {
            answer2 = 1;
            System.out.println("The factorial of " + num2 + " is " + answer2);
        } else if (num2 < 0) {
            System.out.println("The factorial of negative integer cannot be computed.");
        }

    }
}
